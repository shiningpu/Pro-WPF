using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace ResourceLibrary
{
    public class CustomResources
    {
        //定义一个静态属性，让它返回需要使用的正确ComponentResourceKey
        public static ComponentResourceKey SadTileBrush
        {
            get
            {
                return new ComponentResourceKey(
                    typeof(CustomResources), "SadTileBrush");
            }
        }
    }
}
