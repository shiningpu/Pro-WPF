﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace OnlyUseCode
{
    public class Window1:Window
    {
        private Button button1;

        public Window1()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.Width = this.Height = 285;
            this.Left = this.Top = 100;
            this.Title = "Code-Only Window";

            DockPanel panel = new DockPanel();

            button1 = new Button();
            button1.Content = "Please click me";
            button1.Margin = new Thickness(30);

            button1.Click += button1_click;

            IAddChild container = panel;
            container.AddChild(button1);

            container = this;
            container.AddChild(panel);
        }

        private void button1_click(object sender, RoutedEventArgs e)
        {
            button1.Content = "Thank you";
        }
    }
}
